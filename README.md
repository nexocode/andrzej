# README #

Main responsibility of this code is to provide an easy an reliable way to create LINQ to Entities queries directly from URI (or request body).
Inside query string a front application interface can supply database column name, operation (Equals, NotEquals, GreaterThan etc) and parameter to which column should be compared. 
Code takes all these names, operations and operation parameters, and builds LINQ Expression tree which can be easily translated to Sql by entity framework. 
Example URL which could be translated to LINQ query: 

/Orders?OrderDate.ConditionValue=&OrderDate.Operator=&ShipAddress.ConditionValue=&ShipAddress.Operator=&ShipRegion.ConditionValue=R&ShipRegion.Operator=CONTAINS&ShipName.ConditionValue=&ShipName.Operator=&ShippedDate.ConditionValue=08.07.1996&ShippedDate.Operator=GT&OrderBy=OrderID&Order=ASC&PageSize=50

Besides parameterizing operation, pagination and ordering is also possible.

Operation which can be performed (by types):
DateTime, int, float (and similiar simpl types): EQ, NE, GT, GTE, LT, LTE (nullable types supported)
string: EQ, NE, StartsWith, EndsWith, Contains
Guid: EQ, NE
Bool: EQ, NE

### What is this repository for? ###

This application is a demonstration of code I once created in our previous project. 

### How do I get set up? ###

Just run the application. First migration fills out all data (including user)
Make sure you have .\SQLEXPRESS database engine instance. In other case, just change the connection string in web.config in ExampleApplication

Credentials which allow to login to application: 
user@email.com 
!23Password

Then go to the /Orders