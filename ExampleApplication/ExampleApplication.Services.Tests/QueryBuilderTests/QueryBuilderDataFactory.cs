﻿using ExampleApplication.Services.QueryBuilder;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ExampleApplication.Services.Tests.QueryBuilderTests
{
    [TestFixture]
    public class QueryBuilderDataFactory
    {   
        [Test]
        public void QueryBuilderParameters_SearchOrderWithSpecifiedCustomerId()
        {
            OrderQueryParameters parameters = new OrderQueryParameters();
            parameters.CustomerId.ConditionValue = "BLAUS";
            parameters.CustomerId.Operator = "EQ";
            var result = CreateTestCases().ApplyCondition(parameters.CustomerId).UnderlyingQuery.ToList();

            // Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("BLAUS", result.First().CustomerId);
        }

        [Test]
        public void QueryBuilderParameters_SearchOrderWithSpecifiedNotEqualCustomerId()
        {
            OrderQueryParameters parameters = new OrderQueryParameters();
            parameters.CustomerId.ConditionValue = "BLAUS";
            parameters.CustomerId.Operator = "NE";
            var result = CreateTestCases().ApplyCondition(parameters.CustomerId).UnderlyingQuery.ToList();

            // Assert
            Assert.IsTrue(result.Any(), "Expected some results, but got no results");
            Assert.IsTrue(result.All(x => x.CustomerId != "BLAUS"));
        }

        [Test]
        public void QueryBuilderParameters_SearchOrderWithSpecifiedDate()
        {
            var orderDate = new DateTime(1998, 05, 06, 2, 0, 0); 
            OrderQueryParameters parameters = new OrderQueryParameters();
            parameters.OrderDate.ConditionValue = orderDate;
            parameters.OrderDate.Operator = "LT";

            var result = CreateTestCases().ApplyCondition(parameters.OrderDate).UnderlyingQuery.ToList();

            // Assert
            Assert.IsTrue(result.Any(), "Expected some results, but got no results");
            Assert.IsTrue(result.All(x => x.OrderDate < orderDate));
        }

        [Test]
        public void QueryBuilderParameters_SearchWithSpecifiedBool()
        {
            OrderQueryParameters parameters = new OrderQueryParameters();
            parameters.IsShipped.ConditionValue = true;
            parameters.IsShipped.Operator = "EQ";

            var result = CreateTestCases().ApplyCondition(parameters.IsShipped).UnderlyingQuery.ToList();

            // Assert
            Assert.IsTrue(result.Any(), "Expected some results, but got no results");
            Assert.IsTrue(result.All(x => x.IsShipped));
        }

        [Test]
        public void QueryBuilderParameters_SearchWithSpecifiedFreight()
        {
            OrderQueryParameters parameters = new OrderQueryParameters();
            parameters.Freight.ConditionValue = 29.93;
            parameters.Freight.Operator = "GTE";

            var result = CreateTestCases().ApplyCondition(parameters.Freight).UnderlyingQuery.ToList();

            // Assert
            Assert.IsTrue(result.Any(), "Expected some results, but got no results");
            Assert.IsTrue(result.All(x => x.Freight >= 29.93));
        }

        [Test]
        public void QueryBuilderParameters_SearchWithSpecifiedUniqueId()
        {
            OrderQueryParameters parameters = new OrderQueryParameters();
            parameters.UniqueOrderId.ConditionValue = new Guid("116b430d-256e-46eb-9641-a4eabe9c0832");
            parameters.UniqueOrderId.Operator = "EQ";

            var result = CreateTestCases().ApplyCondition(parameters.UniqueOrderId).UnderlyingQuery.ToList();

            // Assert
            Assert.IsTrue(result.Count == 1, "Expected one result, but got few results");
            Assert.IsTrue(result.First().UniqueOrderId == new Guid("116b430d-256e-46eb-9641-a4eabe9c0832"));
        }

        [Test]
        public void QueryBuilderParameters_SearchWithSpecifiedNotEqualUniqueId()
        {
            OrderQueryParameters parameters = new OrderQueryParameters();
            parameters.UniqueOrderId.ConditionValue = new Guid("116b430d-256e-46eb-9641-a4eabe9c0832");
            parameters.UniqueOrderId.Operator = "NE";

            var tcs = CreateTestCases();
            var result = tcs.ApplyCondition(parameters.UniqueOrderId).UnderlyingQuery.ToList();

            // Assert
            Assert.IsTrue(result.Any(), "Expected some results, but got no results");
            Assert.IsTrue(result.First().UniqueOrderId != new Guid("116b430d-256e-46eb-9641-a4eabe9c0832"));
        }

        [Test]
        public void QueryBuilderParameters_SearchWithSpecifiedNullableDate()
        {
            var date = new DateTime(1998, 05, 01, 02, 00, 00);
            OrderQueryParameters parameters = new OrderQueryParameters();
            parameters.ShippedDate.ConditionValue = date;
            parameters.ShippedDate.Operator = "EQ";

            var result = CreateTestCases().ApplyCondition(parameters.ShippedDate).UnderlyingQuery.ToList();

            // Assert
            Assert.IsTrue(result.Any(), "Expected some results, but got no results");
            Assert.IsTrue(result.All(x => x.ShippedDate == date));
        }

        [Test]
        public void QueryBuilderParameters_SearchWithSpecifiedPagination()
        {   
            OrderQueryParameters parameters = new OrderQueryParameters();
            parameters.Page = 2;
            parameters.PageSize = 10;

            var result = CreateTestCases().SkipAndTake(parameters).ToList();

            // Assert
            Assert.IsTrue(result.Any(), "Expected some results, but got no results");
            Assert.IsTrue(result.Count == 10);
        }

        [Test]
        public void QueryBuilderParameters_SearchWithSpecifiedOrderBy()
        {
            OrderQueryParameters parameters = new OrderQueryParameters();
            parameters.OrderBy = "Id";

            var result = CreateTestCases().ApplyOrderBy(parameters).ToList();

            var minId = result.Min(x => x.Id);
            var maxId = result.Max(x => x.Id);

            // Assert
            Assert.IsTrue(result.Any(), "Expected some results, but got no results");
            Assert.AreEqual(minId, result.First().Id);
            Assert.AreEqual(maxId, result.Last().Id);
        }

        [Test]
        public void QueryBuilderParameters_SearchWithSpecifiedOrderByDesc()
        {
            OrderQueryParameters parameters = new OrderQueryParameters();
            parameters.OrderBy = "Id";
            parameters.Order = "DESC";

            var result = CreateTestCases().ApplyOrderBy(parameters).ToList();

            var minId = result.Min(x => x.Id);
            var maxId = result.Max(x => x.Id);

            // Assert
            Assert.IsTrue(result.Any(), "Expected some results, but got no results");
            Assert.AreEqual(maxId, result.First().Id);
            Assert.AreEqual(minId, result.Last().Id);
        }

        private static IQueryable<Order> CreateTestCases()
        {
            string json = File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\QueryBuilderTests\\orders.json");
            return JsonConvert.DeserializeObject<List<Order>>(json).AsQueryable();
        }
    }
}
