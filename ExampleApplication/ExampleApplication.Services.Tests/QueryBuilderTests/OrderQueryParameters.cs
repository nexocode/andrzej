﻿using ExampleApplication.Services.QueryBuilder.Model;
using System;

namespace ExampleApplication.Services.Tests.QueryBuilderTests
{
    public class OrderQueryParameters : QueryBuilderParameters
    {
        public OrderQueryParameters()
        {
            CustomerId = ConditionBuilder<Order>.Create(x => x.CustomerId, string.Empty);
            OrderDate = ConditionBuilder<Order>.Create(x => x.OrderDate, DateTime.Now);
            IsShipped = ConditionBuilder<Order>.Create(x => x.IsShipped, false);
            Freight = ConditionBuilder<Order>.Create(x => x.Freight, 0.00);
            UniqueOrderId = ConditionBuilder<Order>.Create(x => x.UniqueOrderId, Guid.Empty);
            ShippedDate = ConditionBuilder<Order>.Create(x => x.ShippedDate, null);
        }

        public QueryBuilderCondition<string> CustomerId { get; }
        public QueryBuilderCondition<DateTime> OrderDate { get; }
        public QueryBuilderCondition<bool> IsShipped { get; }
        public QueryBuilderCondition<double> Freight { get; }
        public QueryBuilderCondition<Guid> UniqueOrderId { get; }
        public QueryBuilderCondition<DateTime?> ShippedDate { get; }
    }
}
