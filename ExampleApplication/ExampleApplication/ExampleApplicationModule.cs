﻿using Autofac;
using Autofac.Integration.Mvc;
using Microsoft.Owin.Security;
using System.Web;

namespace ExampleApplication
{
    public class ExampleApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Register your MVC controllers. 
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModelBinders(typeof(MvcApplication).Assembly);
            builder.RegisterModelBinderProvider();
            builder.RegisterModule<AutofacWebTypesModule>();
            builder.RegisterSource(new ViewRegistrationSource());
            builder.RegisterFilterProvider();            

            builder.Register<IAuthenticationManager>(c => HttpContext.Current.GetOwinContext().Authentication).InstancePerRequest();
        }
    }
}