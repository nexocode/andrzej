﻿using ExampleApplication.Services;
using ExampleApplication.Services.Models;
using System.Web.Mvc;

namespace ExampleApplication.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        private readonly IOrdersService _ordersService;

        public OrdersController(IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        [HttpGet]
        public ActionResult Index(OrdersQueryParameters queryParameters)
        {
            if(queryParameters == null)
            {
                queryParameters = new OrdersQueryParameters();
            }

            var result = _ordersService.GetOrders(queryParameters);
            queryParameters.Results = result;
            return View(queryParameters);
        }

        [HttpGet]
        public ActionResult OrderDetails(int orderId)
        {
            var orderDetails = _ordersService.GetOrderById(orderId);
            return View(orderDetails);
        }
    }
}