﻿using Autofac;
using Autofac.Integration.Mvc;
using Microsoft.Owin.Security.DataProtection;
using Owin;
using System.Reflection;
using System.Web.Mvc;

namespace ExampleApplication
{
    public class AutofacConfig
    {
        public static void Configure(IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            builder.RegisterAssemblyModules(Assembly.GetCallingAssembly());
            builder.RegisterModule<DataAccess.DataAccessModule>();
            builder.RegisterModule<Services.ServicesModule>();
            builder.Register(x => app.GetDataProtectionProvider()).InstancePerLifetimeScope();


            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}