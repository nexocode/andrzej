﻿using ExampleApplication.Services.QueryBuilder.Helpers;
using ExampleApplication.Services.QueryBuilder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace ExampleApplication.Extensions
{
    public static class QueryBuilderHtmlExtensions
    {
        /// <summary>
        /// Render dropdown for ordering fields
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="helper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString QueryOrderBy<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, QueryBuilderParameters>> expression)
        {
            QueryBuilderParameters parameters = expression.Compile()(helper.ViewData.Model);
            Expression<Func<TModel, string>> orderByPropertyExpression = CreateChildPropertyExpression<TModel, string, QueryBuilderParameters>(expression, nameof(QueryBuilderParameters.OrderBy));
            return helper.DropDownListFor(orderByPropertyExpression, parameters.OrderByAllowedFields.Select(x => new SelectListItem { Text = x, Value = x }), new { @class = "form-control" });
        }

        /// <summary>
        /// Renders order dropdown selector
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="helper"></param>
        /// <param name="expressions"></param>
        /// <returns></returns>
        public static MvcHtmlString QueryOrder<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, QueryBuilderParameters>> expression)
        {
            Expression<Func<TModel, string>> orderPropertyExpression = CreateChildPropertyExpression<TModel, string, QueryBuilderParameters>(expression, nameof(QueryBuilderParameters.Order));
            return helper.DropDownListFor(orderPropertyExpression, QueryBuilderParameters.AllowedOrders.Select(x => new SelectListItem { Text = x, Value = x }), new { @class = "form-control" });
        }

        /// <summary>
        /// Renders pager for query builder
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="helper"></param>
        /// <param name="parametersExpression"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <param name="routeValues"></param>
        /// <returns></returns>
        public static MvcHtmlString QueryPager<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, QueryBuilderParameters>> parametersExpression, string actionName, string controllerName)
        {
            var queryInputParameters = parametersExpression.Compile()(helper.ViewData.Model);
            var currentPage = queryInputParameters.Page;
            var pageCount = queryInputParameters.PageCount;

            StringBuilder sb = new StringBuilder();
            UrlHelper urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            RouteValueDictionary dictionary = GetRouteValuesFromRequest(helper);

            string pageQueryStringKey = CreatePageQueryStringKey(parametersExpression);
            int nextPage = currentPage == pageCount ? currentPage : (currentPage + 1);
            int prevPage = currentPage == 1 ? currentPage : currentPage - 1;
            bool addEndButtons = pageCount != 1;


            sb.Append("<ul class='pagination'>");

            if (addEndButtons)
            {
                dictionary[pageQueryStringKey] = prevPage;
                sb.Append($"<li class='page-item'><a class='page-link' href='{urlHelper.Action(actionName, controllerName, dictionary)}'>Previous</a></li>");
            }

            for (int i = 0; i < pageCount; i++)
            {
                var page = (i + 1);
                dictionary[pageQueryStringKey] = page;
                AppendPagerDiv(sb, urlHelper, page.ToString(), page == currentPage, actionName, controllerName, dictionary);
            }

            if (addEndButtons)
            {
                dictionary[pageQueryStringKey] = nextPage;
                sb.Append($"<li class='page-item'><a class='page-link' href='{urlHelper.Action(actionName, controllerName, dictionary)}'>Next</a></li>");
            }

            sb.Append("</ul>");

            return MvcHtmlString.Create(sb.ToString());
        }

        /// <summary>
        /// Renders total records sentence
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="helper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString QueryTotalrecords<TModel>(this HtmlHelper<TModel> helper, Func<TModel, QueryBuilderParameters> expression)
        {
            var totalrecords = expression(helper.ViewData.Model).TotalRecords;
            return MvcHtmlString.Create($"<p>Total records: {totalrecords}</p>");
        }

        /// <summary>
        /// Renders dropdown for query page size
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="helper"></param>
        /// <param name="pageSizeExpression"></param>
        /// <returns></returns>
        public static MvcHtmlString QueryPageSize<TModel, T>(this HtmlHelper<TModel> helper, Expression<Func<TModel, T>> pageSizeExpression)
        {
            return helper.DropDownListFor(pageSizeExpression, QueryBuilderParameters.AllowedPageSizes.Select(x => new SelectListItem { Text = x.ToString(), Value = x.ToString() }), new { @class = "form-control" });
        }

        /// <summary>
        /// Renders input field for query builder
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="helper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString QueryInputFor<TModel, T>(this HtmlHelper<TModel> helper, Expression<Func<TModel, QueryBuilderCondition<T>>> expression)
        {
            return QueryInputFor(helper, expression, null);
        }

        /// <summary>
        /// Renders input field for query builder
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="helper"></param>
        /// <param name="expression"></param>
        /// <param name="selectListItems"></param>
        /// <returns></returns>
        public static MvcHtmlString QueryInputFor<TModel, T>(this HtmlHelper<TModel> helper, Expression<Func<TModel, QueryBuilderCondition<T>>> expression, IEnumerable<SelectListItem> selectListItems)
        {
            var tType = typeof(T);
            if (tType == typeof(DateTime))
            {
                return helper.TextBoxFor(CreateConditionValueExpression(expression), "{0:yyyy-MM-dd}", new { @class = "datepicker datepicker-inline filter form-control", type = "datetime" });
            }
            else if (tType == typeof(string))
            {
                if (selectListItems != null)
                {
                    return helper.DropDownListFor(CreateConditionValueExpression(expression), selectListItems, new { @class = "form-control" });
                }

                return helper.TextBoxFor(CreateConditionValueExpression(expression), new { @class = "form-control" });
            }
            else if (tType == typeof(bool))
            {
                StringBuilder booleanBuilder = new StringBuilder();
                booleanBuilder.Append($"<label for='{helper.IdFor(expression)}' class='form-check-label'>");
                booleanBuilder.Append(helper.EditorFor(CreateConditionValueExpression(expression), new { htmlAttributes = new { @class = "form-check-input" } }).ToHtmlString());
                booleanBuilder.Append(helper.DisplayNameFor(expression).ToHtmlString());
                booleanBuilder.Append("</label>");

                return MvcHtmlString.Create(booleanBuilder.ToString());
            }
            else
            {
                return helper.EditorFor(CreateConditionValueExpression(expression), new { htmlAttributes = new { @class = "form-control" } });
            }
        }

        public static MvcHtmlString QueryFieldNameFor<TModel, T>(this HtmlHelper<TModel> helper, Expression<Func<TModel, QueryBuilderCondition<T>>> expression)
        {
            return helper.HiddenFor(CreateFieldNameExpression(expression));
        }


        /// <summary>
        /// Renders operators dropdown for specified field
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="helper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString QueryOperatorsFor<TModel, T>(this HtmlHelper<TModel> helper, Expression<Func<TModel, QueryBuilderCondition<T>>> expression)
        {
            var tType = typeof(T);

            if (tType == typeof(DateTime) || tType == typeof(DateTime?))
            {
                return GetDropdown(helper, CreateOperatorExpression(expression), SimpleTypeExpressions.SupportedExpressions.Keys);
            }
            else if (tType == typeof(string))
            {
                return GetDropdown(helper, CreateOperatorExpression(expression), StringExpressions.SupportedExpressions.Keys);
            }
            else if (tType == typeof(Guid) || tType == typeof(Guid?))
            {
                return GetDropdown(helper, CreateOperatorExpression(expression), GuidExpressions.SupportedExpressions.Keys);
            }
            else if (tType == typeof(bool))
            {
                return GetDropdown(helper, CreateOperatorExpression(expression), BooleanExpressions.SupportedExpressions.Keys);
            }
            else
            {
                return GetDropdown(helper, CreateOperatorExpression(expression), SimpleTypeExpressions.SupportedExpressions.Keys);
            }
        }

        /// <summary>
        /// Cuts string to requested length
        /// </summary>
        /// <param name="text"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string CutString(this string text, int length)
        {
            int lengthWithDots = length - 3;

            if (text.Length <= lengthWithDots)
            {
                return text;
            }
            return text.Substring(0, lengthWithDots) + "...";
        }

        private static RouteValueDictionary GetRouteValuesFromRequest<TModel>(HtmlHelper<TModel> helper)
        {
            var dictionary = new RouteValueDictionary();

            var queryString = helper.ViewContext.HttpContext.Request.QueryString;
            foreach (var qsk in queryString.Keys)
            {
                dictionary.Add(qsk.ToString(), queryString[qsk.ToString()]);
            }

            return dictionary;
        }

        private static string CreatePageQueryStringKey<TModel>(Expression<Func<TModel, QueryBuilderParameters>> parametersExpression)
        {
            var pageExpression = Expression.Lambda<Func<TModel, int>>(Expression.Property(parametersExpression.Body, nameof(QueryBuilderParameters.Page)), parametersExpression.Parameters);
            return pageExpression.ToString()
                .Split('>')[1]
                .Replace(parametersExpression.Parameters.FirstOrDefault().Name, "")
                .TrimStart(' ', '.');
        }

        private static void AppendPagerDiv(StringBuilder sb, UrlHelper url, string linkText, bool disable, string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            string liClass = disable ? "page-item disabled" : "page-item";

            sb.AppendLine($"<li class='{liClass}'>");
            sb.Append($"<a class='page-link' href='{url.Action(actionName, controllerName, routeValues)}'>");
            sb.Append(linkText);
            sb.AppendLine("</a>");
            sb.AppendLine("</li>");
        }

        private static Expression<Func<TModel, T2>> CreateChildPropertyExpression<TModel, T2, T1>(Expression<Func<TModel, T1>> expression, string propertyName)
        {
            return Expression.Lambda<Func<TModel, T2>>(Expression.Property(expression.Body, propertyName), expression.Parameters);
        }

        private static Expression<Func<TModel, T>> CreateConditionValueExpression<TModel, T>(Expression<Func<TModel, QueryBuilderCondition<T>>> expression)
        {
            return Expression.Lambda<Func<TModel, T>>(Expression.Property(expression.Body, nameof(QueryBuilderCondition<T>.ConditionValue)), expression.Parameters);
        }

        private static Expression<Func<TModel, string>> CreateFieldNameExpression<TModel, T>(Expression<Func<TModel, QueryBuilderCondition<T>>> expression)
        {
            return Expression.Lambda<Func<TModel, string>>(Expression.Property(expression.Body, nameof(QueryBuilderCondition<T>.FieldName)), expression.Parameters);
        }

        private static Expression<Func<TModel, string>> CreateOperatorExpression<TModel, T>(Expression<Func<TModel, QueryBuilderCondition<T>>> expression)
        {
            return Expression.Lambda<Func<TModel, string>>(Expression.Property(expression.Body, nameof(QueryBuilderCondition<T>.Operator)), expression.Parameters);
        }

        private static MvcHtmlString GetDropdown<TModel>(HtmlHelper<TModel> helper, Expression<Func<TModel, string>> operatorExpression, IEnumerable<string> operatorsList)
        {
            return helper.DropDownListFor(operatorExpression, operatorsList.Select(x => new SelectListItem { Text = OperatorNamesMapper.GetOperatorNiceName(x), Value = x.ToString() }), new { @class = "form-control" });
        }
    }
}