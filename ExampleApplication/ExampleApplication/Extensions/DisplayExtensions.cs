﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace ExampleApplication.Extensions
{
    public static class DisplayExtensions
    {
        /// <summary>
        /// Render dropdown for ordering fields
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="helper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString CustomDisplay<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<dt>");
            sb.Append(helper.DisplayNameFor(expression).ToHtmlString());
            sb.Append("</dt>");
            sb.Append("<dd>");
            sb.Append(helper.DisplayFor(expression).ToHtmlString());
            sb.Append("</dd>");

            return MvcHtmlString.Create(sb.ToString());
        }
    }
}