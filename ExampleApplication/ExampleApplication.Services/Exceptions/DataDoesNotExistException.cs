﻿using System;
using System.Runtime.Serialization;

namespace ExampleApplication.Services
{
    [Serializable]
    public class DataDoesNotExistException : Exception
    {
        public DataDoesNotExistException()
        {
        }

        public DataDoesNotExistException(string message) : base(message)
        {
        }

        public DataDoesNotExistException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DataDoesNotExistException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}