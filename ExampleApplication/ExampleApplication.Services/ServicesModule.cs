﻿using Autofac;

namespace ExampleApplication.Services
{
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<OrdersService>().As<IOrdersService>().InstancePerLifetimeScope();
        }
    }
}
