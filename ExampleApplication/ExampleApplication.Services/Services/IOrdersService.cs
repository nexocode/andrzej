﻿using ExampleApplication.Services.Models;
using System.Collections.Generic;

namespace ExampleApplication.Services
{
    public interface IOrdersService
    {
        IList<OrderViewModel> GetOrders(OrdersQueryParameters queryParameters);
        OrderDetailsViewModel GetOrderById(int orderId);
    }
}
