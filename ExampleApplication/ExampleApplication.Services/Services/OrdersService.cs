﻿using ExampleApplication.DataAccess.Entities;
using ExampleApplication.DataAccess.Providers;
using ExampleApplication.DataAccess.Repositiories;
using ExampleApplication.Services.AutomapperConfig;
using ExampleApplication.Services.Models;
using ExampleApplication.Services.QueryBuilder;
using System.Collections.Generic;
using System.Linq;

namespace ExampleApplication.Services
{
    internal class OrdersService : IOrdersService
    {
        private readonly IOrdersRepository _repository;

        public OrdersService(IOrdersRepository repository)
        {
            _repository = repository;
        }

        public OrderDetailsViewModel GetOrderById(int orderId)
        {
            var result = _repository.GetOrderById(orderId);
            if (result == null)
            {
                throw new DataDoesNotExistException();
            }

            return MapperConfiguration.Mapper.Map<Order, OrderDetailsViewModel>(result);
        }

        public IList<OrderViewModel> GetOrders(OrdersQueryParameters queryParameters)
        {
            var query = _repository.Set<Order>()
                .ApplyCondition(queryParameters.CustomerId)
                .ApplyCondition(queryParameters.RequiredDate)
                .ApplyCondition(queryParameters.ShipAddress)
                .ApplyCondition(queryParameters.ShipName)
                .ApplyCondition(queryParameters.ShippedDate)
                .ApplyCondition(queryParameters.ShipPostalCode)
                .ApplyCondition(queryParameters.ShipRegion)
                .UnderlyingQuery;

            var resultQuery = query
                .ApplyOrderBy(queryParameters)
                .SkipAndTake(queryParameters);

            queryParameters.TotalRecords = _repository.Set<Order>().Count();
            queryParameters.SetPageCount(query.Count());

            return resultQuery.Select(x => MapperConfiguration.Mapper.Map<Order, OrderViewModel>(x)).ToList();
        }
    }
}
