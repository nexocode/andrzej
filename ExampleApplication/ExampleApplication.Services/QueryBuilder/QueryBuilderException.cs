﻿using System;
using System.Runtime.Serialization;

namespace ExampleApplication.Services.QueryBuilder
{
    public class QueryBuilderException : Exception
    {
        public QueryBuilderException()
        {
        }

        public QueryBuilderException(string message) : base(message)
        {
        }

        public QueryBuilderException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected QueryBuilderException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

}
