﻿using ExampleApplication.Services.QueryBuilder.Model;
using System.Collections.Generic;
using System.Linq;

namespace ExampleApplication.Services.QueryBuilder.Validation
{
    public class QueryBuilderParameterValidator
    {
        /// <summary>
        /// List of errors which can occur while 
        /// </summary>
        public IList<string> Errors { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual IReadOnlyList<string> SearchableFields { get; } = new List<string>().AsReadOnly();

        public bool IsValid(QueryBuilderParameters args)
        {
            if(Errors == null)
            {
                Validate(args);
            }

            return Errors.Count == 0;
        }

        public void Validate(QueryBuilderParameters args)
        {
            Errors = new List<string>
            {
                ValidateOrderByWord(args),
                ValidatePageSize(args),
                ValidatePageNumber(args),
            }
            .Where(x => string.IsNullOrEmpty(x) == false)
            .ToList();
        }

        private string ValidateOrderByWord(QueryBuilderParameters args)
        {
            var allowedOrders = QueryBuilderParameters.AllowedOrders;
            if (!allowedOrders.Contains(args.Order))
            {
                return $"Invalid {nameof(QueryBuilderParameters.Order)} value. Allowed values are: {string.Join(",", allowedOrders)}";
            }

            return null;
        }

        private string ValidatePageSize(QueryBuilderParameters args)
        {
            var allowedPageSizes = QueryBuilderParameters.AllowedPageSizes;
            if(!allowedPageSizes.Contains(args.PageSize))
            {
                return $"Invalid {nameof(QueryBuilderParameters.PageSize)} value. Allowed values are {string.Join(",", allowedPageSizes)}";
            }

            return null;
        }

        private string ValidatePageNumber(QueryBuilderParameters args)
        {
            if(args.Page < 1)
            {
                return $"Invalid {nameof(QueryBuilderParameters.Page)}. Value must be greater than 0";
            }

            return null;
        }

        private string ValidateOrderBy(QueryBuilderParameters args)
        {
            if(string.IsNullOrWhiteSpace(args.OrderBy))
            {
                return $"Field OrderBy must be set";
            }

            if(!args.OrderByAllowedFields.Contains(args.OrderBy))
            {
                return $"Invalid {nameof(QueryBuilderParameters.OrderBy)}. The field {args.OrderBy} is not allowed to order by";
            }
            return null;
        }
    }    
}
