﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ExampleApplication.Services.QueryBuilder.Helpers
{
    public static class OperatorNamesMapper
    {
        public static IReadOnlyDictionary<string, string> ExpressionNames { get; }

        static OperatorNamesMapper()
        {
            ExpressionNames = new ReadOnlyDictionary<string, string>(new Dictionary<string, string>()
            {
                { "", "---" },
                { "EQ", "Equal" },
                { "NE", "Not equal" },
                { "LT", "Less than" },
                { "LTE", "Less than or equal" },
                { "GT", "Greater than" },
                { "GTE", "Greater than or equal" },
                { "CONTAINS", "Contains" },
                { "STARTS", "Starts with" },
                { "ENDS", "Ends with" },
            });
        }

        /// <summary>
        /// Maps operator code to nice name
        /// </summary>
        /// <param name="operat0r"></param>
        /// <returns></returns>
        public static string GetOperatorNiceName(string operat0r)
        {
            return ExpressionNames[operat0r];
        }
    }
}
