﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;

namespace ExampleApplication.Services.QueryBuilder.Helpers
{   
    public static class StringExpressions
    {
        static StringExpressions()
        {
            SupportedExpressions = new ReadOnlyDictionary<string, Func<MemberExpression, Expression, Expression>>(
                new Dictionary<string, Func<MemberExpression, Expression, Expression>>
                {
                    { "", null },
                    { "EQ", CreateEquals },
                    { "NE", CreateNotEquals } ,
                    { "CONTAINS", CreateContains },
                    { "STARTS", CreateStartsWith },
                    { "ENDS", CreateEndsWith }
                });
        }

        /// <summary>
        /// Literals supported for string expressions
        /// </summary>
        public static IReadOnlyDictionary<string, Func<MemberExpression, Expression, Expression>> SupportedExpressions { get; }

        /// <summary>
        /// Builds LINQ Expression tree for string 
        /// </summary>
        /// <typeparam name="TEntity">Entity type for SQL Query</typeparam>
        /// <param name="memberExpression">Member selector</param>
        /// <param name="parameterExpression">parameter expression which allows to select member</param>
        /// <param name="constantExpression">constant expression to compare member value to</param>
        /// <param name="operat0r">Operator literal listed in Supported epxressions</param>
        /// <returns>Expression tree which allows to build SQL query</returns>
        public static Expression<Func<TEntity, bool>> GetExpression<TEntity>(MemberExpression member, ParameterExpression parameterExpression, Expression constantExpression, string operat0r)
        {
            if (string.IsNullOrWhiteSpace(operat0r))
            {
                return null;
            }

            var stringType = typeof(string);
            if (!SupportedExpressions.ContainsKey(operat0r))
            {
                throw new QueryBuilderException($"Cannot apply operator {operat0r} to type {stringType.Name}. For type {stringType.Name} supported operators are: {string.Join(",", SupportedExpressions.Keys)}");
            }

            var callExpression = SupportedExpressions[operat0r]?.Invoke(member, constantExpression);
            return callExpression == null ? null : Expression.Lambda<Func<TEntity, bool>>(callExpression, parameterExpression);
        }

        private static Expression CreateEquals(MemberExpression property, Expression constantExpression)
        {
            return Expression.Equal(property, constantExpression);
        }

        private static Expression CreateNotEquals(MemberExpression property, Expression constantExpression)
        {
            return Expression.NotEqual(property, constantExpression);
        }

        private static MethodCallExpression CreateContains(MemberExpression property, Expression constantExpression)
        {
            return CreateStringExpression(property, constantExpression, nameof(string.Contains));
        }

        private static MethodCallExpression CreateStartsWith(MemberExpression property, Expression constantExpression)
        {
            return CreateStringExpression(property, constantExpression, nameof(string.StartsWith));
        }

        private static MethodCallExpression CreateEndsWith(MemberExpression property, Expression constantExpression)
        {
            return CreateStringExpression(property, constantExpression, nameof(string.EndsWith));
        }


        private static MethodCallExpression CreateStringExpression(MemberExpression property, Expression constantExpression, string stringMethodName)
        {
            var methodInfo = typeof(string).GetMethods().FirstOrDefault(x => x.Name == stringMethodName);
            return Expression.Call(property, methodInfo, constantExpression);            
        }
    }
}
