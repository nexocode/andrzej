﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq.Expressions;

namespace ExampleApplication.Services.QueryBuilder.Helpers
{
    public static class BooleanExpressions
    {
        static BooleanExpressions()
        {
            SupportedExpressions = new ReadOnlyDictionary<string, Func<Expression, Expression, BinaryExpression>>(new Dictionary<string, Func<Expression, Expression, BinaryExpression>>
            {
                { "", null },
                { "EQ", Expression.Equal },
                { "NE", Expression.NotEqual }
            });
        }

        /// <summary>
        /// Builds LINQ expression tree for <see cref="bool" /> properties
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="memberExpression"></param>
        /// <param name="parameterExpression"></param>
        /// <param name="constantExpression"></param>
        /// <param name="operat0r"></param>
        /// <returns></returns>
        internal static Expression<Func<TEntity, bool>> GetExpression<TEntity>(MemberExpression memberExpression, ParameterExpression parameterExpression, Expression constantExpression, string operat0r)
        {
            if (string.IsNullOrWhiteSpace(operat0r))
            {
                return null;
            }

            if (!SupportedExpressions.ContainsKey(operat0r))
            {
                throw new QueryBuilderException($"Operator {operat0r} is not supported. Supported operators for {parameterExpression.Type.Name} are: {string.Join(",", SupportedExpressions.Keys)}");
            }

            var comparisonExpression = SupportedExpressions[operat0r]?.Invoke(memberExpression, constantExpression);
            return comparisonExpression == null ? null : Expression.Lambda<Func<TEntity, bool>>(comparisonExpression, parameterExpression);
        }

        public static IReadOnlyDictionary<string, Func<Expression, Expression, BinaryExpression>> SupportedExpressions { get; }
    }
}
