﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq.Expressions;

namespace ExampleApplication.Services.QueryBuilder.Helpers
{
    public static class SimpleTypeExpressions
    {
        static SimpleTypeExpressions()
        {
            SupportedExpressions = new ReadOnlyDictionary<string, Func<Expression, Expression, BinaryExpression>>(new Dictionary<string, Func<Expression, Expression, BinaryExpression>>
            {
                { "", null },
                { "EQ", Expression.Equal },
                { "NE", Expression.NotEqual },
                { "GT", Expression.GreaterThan },
                { "GTE", Expression.GreaterThanOrEqual },
                { "LT", Expression.LessThan },
                { "LTE", Expression.LessThanOrEqual },
            });
        }

        /// <summary>
        /// Creates LINQ Expression tree for comparing simple type properties like <see cref="int"/>, <see cref="DateTime"/> etc.
        /// </summary>
        /// <typeparam name="TEntity">Entity type for SQL Query</typeparam>
        /// <param name="memberExpression">Member selector</param>
        /// <param name="parameterExpression">parameter expression which allows to select member</param>
        /// <param name="constantExpression">constant expression to compare member value to</param>
        /// <param name="operat0r">Operator literal listed in Supported epxressions</param>
        /// <returns>Expression tree which allows to build SQL query</returns>
        internal static Expression<Func<TEntity, bool>> GetExpression<TEntity>(MemberExpression memberExpression, ParameterExpression parameterExpression, Expression constantExpression, string operat0r)
        {
            if (string.IsNullOrWhiteSpace(operat0r))
            {
                return null;
            }

            if (!SupportedExpressions.ContainsKey(operat0r))
            {
                throw new QueryBuilderException($"Operator {operat0r} is not supported. Supported operators for {parameterExpression.Type.Name} are: {string.Join(",", SupportedExpressions.Keys)}");
            }

            var comparisonExpression = SupportedExpressions[operat0r]?.Invoke(memberExpression, constantExpression);
            return comparisonExpression == null ? null : Expression.Lambda<Func<TEntity, bool>>(comparisonExpression, parameterExpression);
        }

        public static IReadOnlyDictionary<string, Func<Expression, Expression, BinaryExpression>> SupportedExpressions { get; }
    }
}
