﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq.Expressions;

namespace ExampleApplication.Services.QueryBuilder.Helpers
{
    public class GuidExpressions
    {
        static GuidExpressions()
        {
            SupportedExpressions = new ReadOnlyDictionary<string, Func<MemberExpression, Expression, BinaryExpression>>(
                new Dictionary<string, Func<MemberExpression, Expression, BinaryExpression>>
                {
                    { "", null },
                    { "EQ", CreateEquals },
                    { "NE", CreateNotEquals }
                });
        }

        public static IReadOnlyDictionary<string, Func<MemberExpression, Expression, BinaryExpression>> SupportedExpressions { get; }

        /// <summary>
        /// Creates LINQ expression tree for <see cref="Guid"/> 
        /// </summary>
        /// <typeparam name="TEntity">Entity type for SQL Query</typeparam>
        /// <param name="memberExpression">Member selector</param>
        /// <param name="parameterExpression">parameter expression which allows to select member</param>
        /// <param name="constantExpression">constant expression to compare member value to</param>
        /// <param name="operat0r">Operator literal listed in Supported epxressions</param>
        /// <returns>Expression tree which allows to build SQL query</returns>
        public static Expression<Func<TEntity, bool>> GetExpression<TEntity>(MemberExpression member, ParameterExpression parameterExpression, Expression constantExpression, string operat0r)
        {
            if (string.IsNullOrWhiteSpace(operat0r))
            {
                return null;
            }

            var guidType = typeof(Guid);
            if (!SupportedExpressions.ContainsKey(operat0r))
            {
                throw new QueryBuilderException($"Cannot apply operator {operat0r} to type {guidType.Name}. For type {guidType.Name} supported operators are: {string.Join(",", SupportedExpressions.Keys)}");
            }

            var callExpression = SupportedExpressions[operat0r]?.Invoke(member, constantExpression);
            return callExpression == null ? null : Expression.Lambda<Func<TEntity, bool>>(callExpression, parameterExpression);
        }

        private static BinaryExpression CreateEquals(MemberExpression property, Expression constantExpression)
        {
            return Expression.Equal(property, constantExpression);
        }

        private static BinaryExpression CreateNotEquals(MemberExpression property, Expression constantExpression)
        {
            return Expression.NotEqual(property, constantExpression);
        }
    }
}
