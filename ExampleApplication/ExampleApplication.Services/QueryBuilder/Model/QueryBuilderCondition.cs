﻿using System;
using System.Linq.Expressions;

namespace ExampleApplication.Services.QueryBuilder.Model
{
    /// <summary>
    /// Where clause condition field 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class QueryBuilderCondition<T>
    {
        private string _operator;

        public string Operator
        {
            get { return _operator; }
            set { _operator = value?.ToUpper(); }
        }

        public string FieldName { get; internal set; }

        public T ConditionValue { get; set; }
    }

    /// <summary>
    /// Static extensions to build conditions dynamically.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public static class ConditionBuilder<TEntity>
    {
        public static QueryBuilderCondition<T> Create<T>(Expression<Func<TEntity, T>> expression)
        {
            return Create(expression, default(T));
        }

        public static QueryBuilderCondition<T> Create<T>(Expression<Func<TEntity, T>> expression, T value)
        {
            string memberName = ((MemberExpression)expression.Body).Member.Name;
            return new QueryBuilderCondition<T>
            {
                FieldName = memberName,
                ConditionValue = value
            };
        }
    }
}
