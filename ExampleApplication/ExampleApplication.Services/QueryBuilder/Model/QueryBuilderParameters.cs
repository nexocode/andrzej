﻿using System;

namespace ExampleApplication.Services.QueryBuilder.Model
{
    public abstract class QueryBuilderParameters
    {
        private string _order = ASC;

        public int Page { get; set; } = 1;

        public string Order
        {
            get { return _order; }
            set { _order = value.ToUpper(); }
        } 
        
        public int PageSize { get; set; } = 50;

        public double PageCount { get; private set; }

        public int TotalRecords { get; set; }

        public string OrderBy { get; set; }
                
        public static int[] AllowedPageSizes => new int[] { 10, 25, 50, 100, };
        
        public static string[] AllowedOrders => new string[] { ASC, DESC };

        public virtual string[] OrderByAllowedFields { get; } = new string[] { "Id" };

        public const string ASC = "ASC";

        public const string DESC = "DESC";

        public void SetPageCount(int count)
        {
            var pageCount = ((float)count / (float)PageSize);
            PageCount = Math.Ceiling(pageCount);            
        }
    }
}
