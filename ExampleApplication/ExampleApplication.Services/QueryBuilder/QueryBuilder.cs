﻿using ExampleApplication.Services.QueryBuilder.Helpers;
using ExampleApplication.Services.QueryBuilder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ExampleApplication.Services.QueryBuilder
{
    public static class QueryBuilderExtensions
    { 
        /// <summary>
        /// Allows to apply OrderBy according to <see cref="QueryBuilderParameters"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IQueryable<T> ApplyOrderBy<T>(this IQueryable<T> query, QueryBuilderParameters args)
        {   
            var selector = args.CreateOrderBySelector<T>();
            if(selector == null)
            {
                return query;
            }

            Func<Func<T, object>, IOrderedEnumerable<T>> orderBy = GetOrderByCallback(query, args);            
            return orderBy(selector).AsQueryable();            
        }

        /// <summary>
        /// Extension which alows to apply Skip and Take parameters according to <see cref="QueryBuilderParameters"/> instance
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IQueryable<T> SkipAndTake<T>(this IQueryable<T> query, QueryBuilderParameters args)
        {
            query = query
                .ApplyOrderBy(args)
                .Skip((args.Page - 1) * args.PageSize)
                .Take(args.PageSize);

            return query;
        }

        /// <summary>
        /// Extension method which allows to build and apply <see cref="QueryBuilderCondition{T}"/> instance to where clause
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static QueryBuilder<TEntity> ApplyCondition<TEntity, T>(this IQueryable<TEntity> query, QueryBuilderCondition<T> condition)
        {
            return new QueryBuilder<TEntity>(query)
                .ApplyCondition(condition);
        }

        /// <summary>
        /// Creates expression which allows ordering by property name
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>Yields <see cref="System.Func{T, TResult}"></see>/> which is treated like order by selector. </returns>
        private static Func<T, object> CreateOrderBySelector<T>(this QueryBuilderParameters parameters)
        {   
            if(string.IsNullOrWhiteSpace(parameters.OrderBy))
            {
                return null;
            }

            ParameterExpression parameterExpression = Expression.Parameter(typeof(T), "x");            
            var propertyInfo = GetPropertyInfo<T>(parameters.OrderBy);
            var property = Expression.Property(parameterExpression, propertyInfo);
            var lambda = Expression.Lambda(property, parameterExpression).Compile();
            Func<T, object> func = x => lambda.DynamicInvoke(x);
            return func;            
        }

        internal static PropertyInfo GetPropertyInfo<T>(string fieldName)
        {
            var type = typeof(T);
            var property = type.GetProperties().FirstOrDefault(p => p.Name.Equals(fieldName, StringComparison.CurrentCultureIgnoreCase));
            if(property == null)
            {
                throw new QueryBuilderException($"Could not find field {fieldName} in type: {type.Name}");
            }

            return property;
        }

        private static Func<Func<T, object>, IOrderedEnumerable<T>> GetOrderByCallback<T>(IEnumerable<T> query, QueryBuilderParameters args)
        {
            if (args.Order == QueryBuilderParameters.DESC)
            {
                return query.OrderByDescending;
            }

            return query.OrderBy;
        }

        private static Func<Func<T, object>, IOrderedEnumerable<T>> GetThenByCallback<T>(IOrderedEnumerable<T> query, QueryBuilderParameters args)
        {
            if (args.Order == QueryBuilderParameters.DESC)
            {
                return query.ThenByDescending;
            }

            return query.ThenBy;
        }
    }

    public class QueryBuilder<TEntity>
    {
        private readonly ParameterExpression _parameterExpression;
        private IQueryable<TEntity> _query;

        public QueryBuilder(IQueryable<TEntity> query)
        {
            _parameterExpression = Expression.Parameter(typeof(TEntity));
            _query = query;
        }

        public QueryBuilder<TEntity> ApplyCondition<T>(QueryBuilderCondition<T> condition)            
        {
            var predicate = GetConditionExpression(condition, _parameterExpression);
            if(predicate != null)
            {
                _query = _query.Where(predicate);
            }

            return this;
        }

        /// <summary>
        /// Query built using parameters
        /// </summary>
        public IQueryable<TEntity> UnderlyingQuery => _query;

        /// <summary>
        /// Creates LINQ expression (also for EF) by which search will be performed
        /// </summary>
        /// <param name="parameterExpression"></param>
        /// <returns></returns>
        private static Expression<Func<TEntity, bool>> GetConditionExpression<T>(QueryBuilderCondition<T> condition, ParameterExpression parameterExpression)            
        {
            if (condition == null)
            {
                return null;
            }

            var propertyInfo = QueryBuilderExtensions.GetPropertyInfo<TEntity>(condition.FieldName);
            var propertyType = propertyInfo.PropertyType;
            var conditionValue = TryCast(propertyType, condition.ConditionValue);
            if(conditionValue == null)
            {
                return null;
            }

            var conditionValueType = conditionValue.GetType();            
            var operat0r = condition.Operator;

            if (propertyType != conditionValueType && !IsNullablePropertyIsComparableWithValue(propertyType, conditionValueType))
            {
                throw new QueryBuilderException($"Cannot compare different types. Type of field: {propertyType.Name}, type of value {conditionValueType.Name}");
            }

            MemberExpression propertyExpression = Expression.Property(parameterExpression, propertyInfo);
            Expression constantExpression = Expression.Constant(conditionValue);

            if (IsNullableProperty(propertyType))
            {
                constantExpression = Expression.Convert(constantExpression, propertyType);
            }

            if (propertyType == typeof(string))
            {
                return StringExpressions.GetExpression<TEntity>(propertyExpression, parameterExpression, constantExpression, operat0r);
            }
            else if (propertyType == typeof(bool))
            {
                return BooleanExpressions.GetExpression<TEntity>(propertyExpression, parameterExpression, constantExpression, operat0r);
            }
            else if (propertyType == typeof(Guid) || propertyType == typeof(Guid?))
            {
                return GuidExpressions.GetExpression<TEntity>(propertyExpression, parameterExpression, constantExpression, operat0r);
            }

            return SimpleTypeExpressions.GetExpression<TEntity>(propertyExpression, parameterExpression, constantExpression, operat0r);
        }

        private static object TryCast(Type propertyType, object value)
        {
            if (propertyType == typeof(DateTime))
            {
                DateTime time;
                string valueString = value.ToString();
                if (!DateTime.TryParse(valueString, out time))
                {
                    throw new QueryBuilderException($"Unable to parse date: {valueString}");
                }

                return time;
            }

            return value;
        }

        private static bool IsNullablePropertyIsComparableWithValue(Type propertyType, Type conditionValueType)
        {
            return IsNullableProperty(propertyType)
                && propertyType.GetGenericArguments()?.FirstOrDefault() == conditionValueType;
        }

        private static bool IsNullableProperty(Type propertyType)
        {
            return propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>);
        }
    }

}
