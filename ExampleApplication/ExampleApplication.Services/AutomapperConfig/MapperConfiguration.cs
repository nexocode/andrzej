﻿using AutoMapper;
using ExampleApplication.DataAccess.Entities;
using ExampleApplication.Services.Models;

namespace ExampleApplication.Services.AutomapperConfig
{
    public static class MapperConfiguration
    {
        static MapperConfiguration()
        {
            Mapper = new Mapper(new AutoMapper.MapperConfiguration(cfg => ConfigureMapper(cfg)));
        }

        public static IMapper Mapper { get; }

        private static void ConfigureMapper(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Order, OrderViewModel>();
            cfg.CreateMap<Employee, EmployeeViewModel>();
            cfg.CreateMap<OrderDetail, OrderDetailViewModel>()
                .ForMember(x => x.ProductName, x => x.MapFrom(y => y.Product.ProductName));
            cfg.CreateMap<Order, OrderDetailsViewModel>();
        }
    }
}
