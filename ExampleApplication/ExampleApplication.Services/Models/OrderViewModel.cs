﻿using System;

namespace ExampleApplication.Services.Models
{
    public class OrderViewModel
    {
        public string OrderID { get; set; }
        public string CustomerId { get; set; }
        public string ShipRegion { get; set; }
        public string ShipAddress { get; set; }
        public string ShipName { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
    }
}
