﻿using ExampleApplication.DataAccess.Entities;
using ExampleApplication.Services.QueryBuilder.Model;
using System;
using System.Collections.Generic;

namespace ExampleApplication.Services.Models
{
    public class OrdersQueryParameters : QueryBuilderParameters
    {
        public OrdersQueryParameters()
        {
            CustomerId = ConditionBuilder<Order>.Create(x => x.CustomerID, string.Empty);
            ShipRegion = ConditionBuilder<Order>.Create(x => x.ShipRegion, string.Empty);
            ShipAddress = ConditionBuilder<Order>.Create(x => x.ShipAddress, string.Empty);
            ShipPostalCode = ConditionBuilder<Order>.Create(x => x.ShipCountry, string.Empty);
            ShipName = ConditionBuilder<Order>.Create(x => x.ShipName, string.Empty);
            OrderDate = ConditionBuilder<Order>.Create(x => x.OrderDate, null);
            RequiredDate = ConditionBuilder<Order>.Create(x => x.RequiredDate, null);
            ShippedDate = ConditionBuilder<Order>.Create(x => x.ShippedDate, null);

            OrderBy = nameof(DataAccess.Entities.Order.OrderID);
        }

        public QueryBuilderCondition<string> CustomerId { get; }
        public QueryBuilderCondition<string> ShipRegion { get; }
        public QueryBuilderCondition<string> ShipAddress { get; }
        public QueryBuilderCondition<string> ShipPostalCode { get; }
        public QueryBuilderCondition<string> ShipName { get; }
        public QueryBuilderCondition<DateTime?> OrderDate { get; }
        public QueryBuilderCondition<DateTime?> RequiredDate { get; }
        public QueryBuilderCondition<DateTime?> ShippedDate { get; }

        public override string[] OrderByAllowedFields => new string[] { nameof(DataAccess.Entities.Order.OrderID), nameof(CustomerId), nameof(ShipRegion), nameof(ShipAddress), nameof(ShipPostalCode), nameof(ShipName), nameof(OrderDate), nameof(RequiredDate), nameof(ShippedDate) };

        public IList<OrderViewModel> Results { get; set; }
    }
}
