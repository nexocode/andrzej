﻿namespace ExampleApplication.Services.Models
{
    public class OrderDetailViewModel
    {
        public string ProductName { get; set; }

        public decimal UnitPrice { get; set; }

        public short Quantity { get; set; }

        public float Discount { get; set; }
    }
}