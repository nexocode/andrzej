﻿using System.Linq;

namespace ExampleApplication.DataAccess.Providers
{
    public interface IDbContextProvider
    {
        IQueryable<T> Set<T>() where T : class;

        void Add<T>(T item) where T : class;

        int SaveChanges();
    }
}
