﻿using System.Linq;

namespace ExampleApplication.DataAccess.Providers
{
    internal class DbContextProvider : IDbContextProvider
    {
        private readonly ApplicationDbContext _context;

        public DbContextProvider(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add<T>(T item) where T : class
        {
            _context.Set<T>().Add(item);
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public IQueryable<T> Set<T>() where T : class
        {
            return _context.Set<T>().AsQueryable();
        }        
    }
}
