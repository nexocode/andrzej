﻿using ExampleApplication.DataAccess.Entities;
using ExampleApplication.DataAccess.Providers;
using System.Linq;
using System.Data.Entity;

namespace ExampleApplication.DataAccess.Repositiories
{
    public interface IOrdersRepository : IDbContextProvider
    {
        Order GetOrderById(int orderId);
    }

    internal class OrdersRepository : RepositoryBase, IOrdersRepository
    {
        public OrdersRepository(ApplicationDbContext context) : base(context)
        {
        }

        public Order GetOrderById(int orderId)
        {
            return Set<OrderDetail>()
                .Include(x => x.Orders)
                .Include(x => x.Product)
                .FirstOrDefault(x => x.OrderID == orderId)?.Orders;
        }
    }

}
