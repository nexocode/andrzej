﻿using ExampleApplication.DataAccess.Providers;
using System.Linq;

namespace ExampleApplication.DataAccess.Repositiories
{
    internal abstract class RepositoryBase : IDbContextProvider
    {
        private readonly ApplicationDbContext _context;

        protected RepositoryBase(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add<T>(T item) where T : class
        {
            _context.Set<T>().Add(item);
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public IQueryable<T> Set<T>() where T : class
        {
            return _context.Set<T>().AsQueryable();
        }
    }
}
