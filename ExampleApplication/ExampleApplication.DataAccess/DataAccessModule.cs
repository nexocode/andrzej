﻿using Autofac;
using Microsoft.Owin.Security.DataProtection;
using ExampleApplication.DataAccess.Providers;
using ExampleApplication.DataAccess.Repositiories;

namespace ExampleApplication.DataAccess
{
    public class DataAccessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ApplicationDbContext>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<ApplicationSignInManager>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<DbContextProvider>().As<IDbContextProvider>().InstancePerLifetimeScope();
            builder.Register(x => ApplicationUserManager.Create(x.Resolve<IDataProtectionProvider>(), x.Resolve<ApplicationDbContext>())).InstancePerLifetimeScope();

            builder.RegisterType<OrdersRepository>().As<IOrdersRepository>().InstancePerLifetimeScope();
        }
    }
}
